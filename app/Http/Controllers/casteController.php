<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class casteController extends Controller
{
    public function create(){
        return view ('caste.create');
    }
    public function store (Request $request){
    $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
    ]);
    DB::table('caste')->insert(
        [
            'nama' => $request['nama'], 
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]
    );
    return redirect('/caste/create');
}
public function index(){
    $caste = DB::table('caste')->get();
    return view ('caste.index', compact ('caste'));
}
public function show($id){
    $caste = DB::table('caste') -> where ('id', $id)->first();
    return view ('caste.show', compact ('caste_id'));
}
public function edit($id){
    $caste = DB::table('caste') -> where ('id', $id)->first();
    return view ('caste.edit', compact ('caste_id'));
}
}





