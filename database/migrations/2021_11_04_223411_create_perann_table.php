<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerannTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perann', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 45);
            $table->unsignedBigInteger('pelem_id');
            $table->foreign('pelem_id')->references('id')->on('pelem');
            $table->unsignedBigInteger('caste_id');
            $table->foreign('caste_id')->references('id')->on('caste');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perann');
    }
}
