<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/caste/create', 'casteController@create');
Route::post('/caste', 'casteController@store');
Route::get('/caste', 'casteController@index');
Route::get('/caste/{caste_id}', 'casteController@show');
Route::get('/caste/{caste_id}/edit', 'casteController@edit');
?>
